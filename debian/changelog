libclass-dbi-fromform-perl (0.04-4) UNRELEASED; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 06 Jan 2013 21:58:25 +0100

libclass-dbi-fromform-perl (0.04-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 18:30:36 +0100

libclass-dbi-fromform-perl (0.04-3) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.
  * Remove Florian Ragwitz from Uploaders (closes: #523158).

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.
  * Use source format 3.0 (quilt).
  * Use tiny debian/rules with debhelper 8.
  * Convert debian/copyright to proposed machine-readable format.
  * Bump Standards-Version to 3.9.2.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 16 Sep 2011 21:50:03 +0200

libclass-dbi-fromform-perl (0.04-2) unstable; urgency=low

  * bugfix release
  * debian/rules: fix FTBS bug (closes: #467712)
  * debian/control:
   + Add Debian Perl Group Headers
   + Standards-Version upgraded to 3.7.3 (no changes)
  * debian/compat: increased to 5 (updated debhelper dependency)

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Mon, 17 Mar 2008 13:02:40 +0100

libclass-dbi-fromform-perl (0.04-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue,  7 Mar 2006 15:09:50 +0100

libclass-dbi-fromform-perl (0.03-2) unstable; urgency=low

  * debian/control - added me to uploaders
  * debian/watch - added file

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 10 Jan 2006 11:53:25 +0100

libclass-dbi-fromform-perl (0.03-1) unstable; urgency=low

  * Initial release.

 -- Florian Ragwitz <rafl@debianforum.de>  Wed, 10 Aug 2005 12:25:51 +0200
